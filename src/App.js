import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Login from "./Views/Login";
import Register from "./Views/Register";
import Home from "./Views/Home";
import Header from "./Views/header/Header";
import {firebaseConfig} from "./env";
import firebase from "firebase";
import "firebase/auth";
import {FirebaseAuthConsumer, FirebaseAuthProvider,} from "@react-firebase/auth"
import DocumentList from "./Views/DocumentList";
import Document from "./Views/Document";
import About from "./Views/About";
import Messenger from "./Views/messenger";

function App() {


    return (
        <BrowserRouter>
            <FirebaseAuthProvider firebase={firebase} {...firebaseConfig}>
                <Header/>
                <Switch>
                    <FirebaseAuthConsumer>
                        {({isSignedIn}) => {
                            if (!isSignedIn) {
                                return (<>
                                        <Route exact={true} path={"/login"} component={Login}/>
                                        <Route exact={true} path={"/register"} component={Register}/>
                                        <Route exact={true} path={"/"} component={Home}/>
                                        <Route exact={true} path={"/about"} component={About}/>
                                    </>
                                )
                            } else {
                                return <>
                                    <Route exact={true} path={"/"} component={Home}/>
                                    <Route exact={true} path={"/about"} component={About}/>
                                    <Route exact={true} path={"/documents"} component={DocumentList}/>
                                    <Route exact={true} path={"/document/:id"} component={Document}/>
                                </>
                            }
                        }}
                    </FirebaseAuthConsumer>
                </Switch>
            </FirebaseAuthProvider>
        </BrowserRouter>
    );
}

export default App;
