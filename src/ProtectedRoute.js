import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {FirebaseAuthConsumer} from "@react-firebase/auth";

const ProtectedRoute = ({ component: Component, ...rest }) => {

    console.log("plouf")

    return (
        <FirebaseAuthConsumer>
            {({isSignedIn}) => {
                console.log(isSignedIn)
                if (isSignedIn) {
                   return <Route exact={true} {...rest} render={
                        props => {
                            return <Component {...rest} {...props} />
                        }
                    } />
                }else{
                    return <Redirect to={'/login'} />
                }
            }}
        </FirebaseAuthConsumer>
    )
}

export default ProtectedRoute;
