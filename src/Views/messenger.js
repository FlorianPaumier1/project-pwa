import {useEffect, useState} from "react";
import firebase from "firebase";

const Messenger = () => {
    const [messages, setMessages] = useState({
        users: [],
        messages: []
    })
    const [message, setMessage] = useState("")
    const [user, setUser] = useState("")

    const [documentObject, setDocumentObject] = useState([])
    let editData = null

    useEffect(() => {
        setUser(firebase.auth().currentUser)

        firebase.database().ref("messages/").on("value", (snap) => {
            let documentData = snap.val()
            if(documentData){
                const users = documentData.users
                const exist = users.indexOf(user.email)

                if (exist === -1) {
                    users.push(user.email)
                    documentData.users = users
                    setMessages(documentData)
                }

                editData = documentData
                setMessages(documentData)
                push(editData)
            }else{
                push(messages)
            }
        });

        return async () => {
            const user = firebase.auth().currentUser
            if(editData){
                const users = editData.users
                const exist = users.indexOf(user.email)
                if (exist > -1) {
                    users.splice(exist, 1)
                    editData.users = users
                    firebase.database().ref("messages").off()
                    await editProps(`messages/users`, editData.users)
                }
            }
        }
    }, [])

    const add = (e) => {
        e.preventDefault()
        console.log(message)

        messages.messages = [...messages.messages, {
            user: user.email,
            message: message,
            createdAt: (new Date()).toString()
        }]
        push(messages)
        setMessages(messages)
        setMessage("")
    }

    async function push(item) {
        await firebase.database().ref(`user_documents/${item.id}`).set(item);
    }

    async function editProps(path, item) {
        await firebase.database().ref(path).set(item);
    }

    return (
        <>
            <section>
                {messages.users.join(",")}
            </section>
            <section>
                <ul>
                    {messages.messages.map(message => {
                        return (<div>
                            {message.user} <br/>
                            {message.createdAt} <br/>
                            {message.message}
                        </div>)
                    })}
                </ul>
                <form onSubmit={add}>
                    <input type="text" onChange={(event) => setMessage(event.target.value)}/>
                    <button type="submit">Valider</button>
                </form>
            </section>
        </>
    )
}

export default Messenger
