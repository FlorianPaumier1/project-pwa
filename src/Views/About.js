const About = () => {
    const container = {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "16px",
        backgroundColor: "white",
    }
    return (
        <div style={container}>
        <section>
            <h1>Projet ESGI PWA</h1>
            <p>Réalisation d'une PWA sur le thème de Google Docs, avec édition collaborative</p>
            <hr/>
                <p>## Fonctionnalités demandé :</p>
                <ul>
                    <li>* Authentification (Inscription/Connexion)</li>
                    <li>* Implémenter un WYSIWYG (HTML/Markdown)</li>
                    <li>* Sauvegarde de la donnée dans Firebase</li>
                    <li>* Édition collaborative</li>
                    <li>* Édition hors ligne</li>
                    <li>* Synchronisation online/offline</li>
                </ul>
                <p>## Fonctionnalités bonus :</p>
                <ul>
                    <li>* Affichage du curseurs des participants</li>
                    <li>* Chat des participants</li>
                </ul>
                <p>## Notation :</p>
                <ul>
                    <li>* 2.5 points par fonctionnalités</li>
                    <li>* 5 points pour les performances (Score Lighthouse)</li>
                </ul>
        </section>
        </div>
)
}

export default About;
