import { useEffect, useState } from "react";
import { FirebaseDatabaseNode, FirebaseDatabaseProvider } from "@react-firebase/database";
import firebase from "firebase";
import { firebaseConfig } from "../env";

const DocumentList = () => {
    const [documentObject, setDocumentObject] = useState()
    const [key, setKey] = useState()
    const [user, setUser] = useState()
    const [database, setDatabase] = useState()

    useEffect(() => {
        setDatabase(firebase.database())
        setUser(firebase.auth().currentUser)
    }, [])

    const handleChange = (e) => {
        setDocumentObject({
            'title': e.target.value,
            'content': "",
            'users': JSON.stringify([]),
            'author': user.email,
            'created_on': (new Date()).toString(),
            'updated_on': (new Date()).toString()
        });
    }

    async function createDocument(e) {
        e.preventDefault();
        let keyElement = await database.ref().child("user_documents/").push().key;
        documentObject.id = keyElement
        database.ref(`user_documents//${keyElement}`).set(documentObject);
        setDocumentObject(documentObject)
        setKey(keyElement);
    }

    const container = {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "16px",
        backgroundColor: "white",
    }

    const input = {
        width: "100%",
        padding: "15px",
        margin: "5px 0 22px 0",
        display: "inline-block",
        border: "none",
        background: "#f1f1f1"
    }

    const createbtn = {
        backgroundColor: "#00798c",
        color: "white",
        padding: "16px 20px",
        margin: "8px 0",
        border: "none",
        cursor: "pointer",
        width: "100%",
        opacity: "0.9",
        borderRadius: "4px"
    }

    const btndelete = {
        backgroundColor: "#b5300e",
        color: "white",
        padding: "5px 20px",
        margin: "8px 0",
        border: "none",
        cursor: "pointer",
        width: "100%",
        opacity: "0.9",
        borderRadius: "4px"
    }
    

    const main = {
        padding: "0.25rem 0.5rem"
    }
    const main_h1 = {
        fontWeight: "500",
        fontSize: "1.2rem"
    }
    const main_p = {
        textAlign: "right",
        paddingTop: "0.25rem",
        fontSize: "0.75rem",
        color: "#6D6D6D"
    }

    const a = {
        display: "block",
        textDecoration: "none",
        color: "inherit",
        border: "1px solid",
        borderRadius: "10px",
        marginBottom: "10px",
        cursor: "pointer"
    }

    const center_div = {
        margin: "0 auto",
        width: "50%"
    }

    const deleteValue = (e,item) => {
        e.preventDefault()
        database.ref("user_documents/"+item).remove()
    }

    return (
        <FirebaseDatabaseProvider firebase={firebase} {...firebaseConfig}>
            <section>
                <div>
                    <div>
                        <form onSubmit={createDocument}>
                            <div style={container}>
                                <input style={input} type="text" onChange={handleChange} name={"title"} placeholder="Nom du nouveau document" required />
                                <button style={createbtn}>
                                    Créer
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <div style={center_div}>
                <FirebaseDatabaseNode
                    path="user_documents/"
                    // orderByKey
                    orderByValue={"created_on"}
                >
                    {d => {
                        if (d.value) {
                            return Object.keys(d.value).map((key) => {
                                var cdate = (new Date(d.value[key].created_on)).toString();
                                return <a href={"/document/" + key} style={a}>
                                    <main style={main}>
                                        <h1 style={main_h1} key={key}>{d.value[key].title}</h1>
                                        <p style={main_p}>{cdate}</p>
                                        <button style={btndelete} onClick={(e) => deleteValue(e,key)}>Supprimer</button>
                                    </main>
                                </a>
                            })
                        }
                        return "";
                    }}
                </FirebaseDatabaseNode>
            </div>
        </FirebaseDatabaseProvider>
    )
}

export default DocumentList;
