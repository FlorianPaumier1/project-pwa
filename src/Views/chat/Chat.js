import React, { useEffect, useState } from 'react';
import './chat.scss';
import TextareaAutosize from 'react-textarea-autosize';
import firebase from "firebase";

export const Chat = ({isOpen, openChat, user}) => {
    const [msg, setMsg] = useState("");

    const [listMsg, setListMsg] = useState([])

    const handleSubmit = (e) => {
        e.preventDefault();
        const userMsg = firebase.auth().currentUser
        const messages = [
            ...listMsg,
            {
                content: msg,
                createdAt: new Date().toJSON(),
                userId: userMsg.email
            }
        ]
        setListMsg(messages);
        push({
            content: msg,
            createdAt: new Date().toJSON(),
            userId: user.email
        })
        setMsg("");
    }

    useEffect(() => {
        firebase.database().ref("messages/").on("value", (snap) => {
            let documentData = snap.val()
            if (documentData) {
                setListMsg(Object.values(documentData))
            }
        });
    }, [])

    async function push(item) {
        let keyElement = await firebase.database().ref().child("messages/").push().key;
        firebase.database().ref(`messages/${keyElement}`).set(item);
    }

    const styles = {
        chatContainer: {
            display: "none",
            flexDirection: "column",
            width: "20vw",
            alignItems: "center",
            borderRight: "1px solid grey",
            height: "100vh",
            maxHeight: "100vh",
            justifyContent: "flex-end",
            boxSizing: "border-box",
            position: "fixed",
            top: 0,
            zIndex: 50,
            overflow: 'hidden',
            paddingTop: 56
        },
        chatClose: {
            position: 'absolute',
            zIndex: 100,
            top: 18,
            left: 15
        },
        chatHeader: {
            padding: 15,
            position: "absolute",
            top: 0,
            left: 0,
            width: '20vw',
            zIndex: 105
        },
        chatContent: {
            width: "100%",
            paddingBottom: 15,
            overflowY: "auto"
        },
        chatFooter: {
            width: "100%",
            padding: 15
        },
        msgBlock: {
            backgroundColor: 'lightgrey',
            padding: 4,
            margin: "15px 15px 0",
            maxWidth: "75%",
            color: "#fff",
            display: "inline-flex"
        }
    }

    if (!user) return <></>

    return (
        <div id='chatbox' className={`${isOpen ? "open" : ""}`} style={styles.chatContainer}>
            <div className="chat-header" style={styles.chatHeader}>
                <i
                    className="fas fa-times"
                    style={styles.chatClose}
                    onClick={() => openChat(false)}
                ></i>
                <span>Chatbox &nbsp; <i className="far fa-comment-alt"></i></span>
            </div>
            <div className="chat-content" style={styles.chatContent}>
                {/* changer condition userId */}
                {listMsg.map(m =>
                    <div className="msg-row" style={{
                        display: "flex",
                        justifyContent: m.userId === user.email ? "flex-end" : "flex-start"
                    }}>
                        <div
                            className={`msgBlock ${m.userId === user.email ? "self" : ""}`}
                            style={styles.msgBlock}
                        >
                            <span className="msgText" style={{ padding: 4, overflowWrap: "break-word", maxWidth: "88%" }}>{m.content}</span>
                            <span className="msgDate"
                                style={{ fontSize: 11, alignSelf: "flex-end", textAlign: "right" }}>{new Date(m.createdAt).getHours()}:{new Date(m.createdAt).getMinutes()}</span>
                        </div>
                    </div>
                )}
            </div>
            <div className="chat-footer" style={styles.chatFooter}>
                <TextareaAutosize
                    style={{ boxSizing: 'border-box' }}
                    maxRows={4}
                    value={msg}
                    onChange={e => setMsg(e.target.value)}
                    className="msgTextarea"
                />
                <div className="btn-div">
                    <button type="submit" className="send" onClick={msg.length !== 0 ? handleSubmit : null}>
                        <i className="fas fa-paper-plane"></i>
                    </button>
                </div>
            </div>
        </div>
    );
}
