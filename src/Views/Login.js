import {useEffect, useRef, useState} from "react";
import firebase from "firebase";
import {Redirect} from "react-router-dom";

const Login = () => {
    const [isRegistered, setIsRegistered] = useState(false);

    let form = useRef(null);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const form_data = new FormData(form.current);
        let payload = {};
        form_data.forEach(function (value, key) {
            payload[key] = value;
        });

        firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                setIsRegistered(true)
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                alert(errorMessage)
            });

    };

    useEffect(() => {
        const user = firebase.auth().currentUser
        setIsRegistered(user !== null)
    }, [])

    if (isRegistered) {
        return <Redirect to={"/"}/>
    }

    const container = {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "16px",
        backgroundColor: "white",
    }
    const input = {
        width: "100%",
        padding: "15px",
        margin: "5px 0 22px 0",
        display: "inline-block",
        border: "none",
        background: "#f1f1f1"
    }

    const loginbtn= {
        backgroundColor: "#705E78",
        color: "white",
        padding: "16px 20px",
        margin: "8px 0",
        border: "none",
        cursor: "pointer",
        width: "100%",
        opacity: "0.9",
        borderRadius: "4px"
    }

    const hr = {
        border: "1px solid #f1f1f1",
        marginBottom:" 25px"
    }

    return (
        <form onSubmit={handleSubmit} ref={form}>
            <div style={container}>
                <h1>Se connecter</h1>
                <hr style={hr}/>
                <input style={input} type="text" placeholder={"email"} name={"email"} id={"email"}/>
                <input style={input} type="password" placeholder={"password"} name={"password"} id={"password"}/>
                <button style={loginbtn} type={"submit"}>Submit</button>
            </div>
        </form>
    )

}

export default Login
