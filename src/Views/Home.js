const Home = () => {
    const container = {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "16px",
        backgroundColor: "white",
    }
    const h1 = {
        margin: "5px"
    }
    return (
        <div style={container}>
            <h1 style={h1}>Bienvenue sur Doc ESGI - PWA App</h1>
        </div>
    )
}

export default Home
