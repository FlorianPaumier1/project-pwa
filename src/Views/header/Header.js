import React, {useEffect, useState} from 'react';
import { NavLink } from "react-router-dom";
import { FirebaseAuthConsumer } from "@react-firebase/auth";
import firebase from 'firebase';
import './header.scss';
import { Chat } from '../chat/Chat';

const Header = () => {

    const [isOpen, openChat] = useState(false);
    const [user, setUser] = useState(null);

    useEffect(() => {
        setUser(firebase.auth().currentUser)
    }, [])

    const styles = {
        header: {
            textAlign: "center",
            position: "sticky",
            top: 0,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "56px",
            zIndex: 10
        },
        nav: {
            padding: 0,
            margin: 0,
            textAlign: "center"
        },
        navLi: {
            display: "inline",
            listStyle: "none"
        },
        navA: {
            display: "inline-block",
            margin: "0 30px",
            textDecoration: "none",
            color: "inherit"
        },
        image: {
            width: "45px",
            height: "auto"
        },
        noStyleButton: {
            background: "none",
            color: "inherit",
            border: "none",
            padding: "0",
            font: "inherit",
            cursor: "pointer",
            outline: "inherit"
        }
    }

    return (
        <FirebaseAuthConsumer>
            {({ isSignedIn, user }) => {
                return (
                    <>
                    <Chat isOpen={isOpen} openChat={openChat} user={user}/>
                    <header style={styles.header}>
                        {user && (
                            <i
                                className="far fa-comment-alt"
                                onClick={() => openChat(true)}
                            />
                        )}

                            <ul style={styles.nav}>
                                <li style={styles.navLi}><NavLink style={styles.navA} to={"/"}>Menu</NavLink></li>
                                <li style={styles.navLi}><NavLink style={styles.navA} to={"/about"}>A propos</NavLink></li>
                                {isSignedIn && (<>
                                    <li style={styles.navLi}><NavLink style={styles.navA} to={"/documents"}>Documents</NavLink></li>
                                    <li style={styles.navLi}>
                                        <button style={styles.noStyleButton}
                                            onClick={() => {
                                                firebase.auth().signOut().then(() => {
                                                    window.location.href = '/'
                                                });
                                            }}
                                        >
                                            Déconnexion
                                    </button>
                                    </li>
                                </>)}
                                {!isSignedIn && (
                                    <>
                                        <li style={styles.navLi}><NavLink style={styles.navA} to={"/login"}>Connexion</NavLink></li>
                                        <li style={styles.navLi}><NavLink style={styles.navA} to={"/register"}>S'inscrire</NavLink></li>
                                    </>
                                )
                                }

                            </ul>

                        </header>
                    </>
                )
            }}
        </FirebaseAuthConsumer>
    )
}

export default Header
