import { NavLink } from "react-router-dom";
import { FirebaseAuthConsumer } from "@react-firebase/auth";
import firebase from 'firebase';
import {useEffect, useState} from "react";

const Header = () => {

    const head_er = {
        textAlign: "center",
        position: "sticky",
        top: 0,
        backgroundColor: "#812F33",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "56px",
        color: "#ffffff",
        zIndex: 10
    }

    const nav = {
        padding: 0,
        margin: 0,
        textAlign: "center"
    }

    const nav_li = {
        display: "inline",
        listStyle: "none"
    }

    const nav_a = {
        display: "inline-block",
        margin: "0 30px",
        textDecoration: "none",
        color: "inherit"
    }
    const image = {
        width: "45px",
        height: "auto"
    }

    const nostylebutton = {
        background: "none",
        color: "inherit",
        border: "none",
        padding: "0",
        font: "inherit",
        cursor: "pointer",
        outline: "inherit"
    }

    const [database, setDatabase] = useState()
    const [user, setUser] = useState()

    const userLog = async () => {
        let keyElement = await database.ref().child("user_logged/").push().key;
        database.ref(`user_documents//${keyElement}`).set(user);
    }

    useEffect(() => {
        setDatabase(firebase.database())
        setUser(firebase.auth().currentUser)
        return () => {
            return ""
        }
    }, [])
    return (
        <FirebaseAuthConsumer>
            {({ isSignedIn, user }) => {
                return (
                    <header style={head_er}>
                        <ul style={nav}>
                            <li style={nav_li}><NavLink style={nav_a} to={"/"}>Menu</NavLink></li>
                            <li style={nav_li}><NavLink style={nav_a} to={"/about"}>A propos</NavLink></li>
                            {isSignedIn && (<>
                                <li style={nav_li}><NavLink style={nav_a} to={"/documents"}>Documents</NavLink></li>
                                <li style={nav_li}>
                                    <button style={nostylebutton}
                                        onClick={() => {
                                            firebase.auth().signOut().then(() => {
                                                window.location.href = '/'
                                            });
                                        }}
                                    >
                                    Déconnexion
                                    </button>
                                </li>
                            </>)}
                            {!isSignedIn && (
                                <>
                                    <li style={nav_li}><NavLink style={nav_a} to={"/login"}>Connexion</NavLink></li>
                                    <li style={nav_li}><NavLink style={nav_a} to={"/register"}>S'inscrire</NavLink></li>
                                </>
                            )
                            }

                        </ul>

                    </header>
                )
            }}
        </FirebaseAuthConsumer>
    )
}

export default Header
