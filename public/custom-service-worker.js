importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js')

if (workbox) {
    console.log(`Service worker chargé avec succes  🎉`); 
    // Faire au + simple ( règle numéro 15 )
    workbox.routing.registerRoute(
        ({ request }) => request.destination === 'image',
        new workbox.strategies.CacheFirst()
    );

} else {
    console.log(`Service worker non chargé 😬`);
}